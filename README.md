# Elm Tutorial Solutions

Because the exercises of the [official Elm tutorials](https://guide.elm-lang.org/) got hard, I tried to find solutions on the internet, but I only found [something old](https://github.com/msachi/intro-to-elm-tutorial), which doesn't compile anymore. So I decided to make it by my own.

The solutions are compatible with **ELM 0.19.1** (2022-08-30).

All solutions work with the [official browser compiler](https://elm-lang.org/try) and the files contain comments on what I've changed.

**Disclaimer:** I am not a professional Elm programmer. I started to learn Elm a few days ago (August 2022). Even if I know other programming languages, it doesn't mean the solutions here are the best. But if I will stay with Elm and will know better solutions, I will improve them. And if I will not update solutions it could mean I am not interested in Elm anymore and found something better.
